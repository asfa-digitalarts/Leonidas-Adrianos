let fishparts;
let heldFishPart;
let imagePos;
let completed = false;

function setup() {
  createCanvas(2000, 2000);
  alert("Hello there");
  alert("Use the image parts on the right to complete the puzzle");
  heldFishPart = null;
  fishparts = [];

  fishparts.push(new FishPart("puzzle/1.png", 640, 250));
  fishparts.push(new FishPart("puzzle/2.png", 790, 340));
  fishparts.push(new FishPart("puzzle/3.png", 860, 630));
  fishparts.push(new FishPart("puzzle/4.png", 970, 266));
  fishparts.push(new FishPart("puzzle/5.png", 1140, 450));
  fishparts.push(new FishPart("puzzle/6.png", 970, 40));
  fishparts.push(new FishPart("puzzle/7.png", 640, 350));
  fishparts.push(new FishPart("puzzle/8.png", 950, 170));
  fishparts.push(new FishPart("puzzle/9.png", 840, 40));
  fishparts.push(new FishPart("puzzle/10.png", 735, 390));
  fishparts.push(new FishPart("puzzle/11.png", 1140, 310));
  fishparts.push(new FishPart("puzzle/12.png", 950, 560));
  fishparts.push(new FishPart("puzzle/13.png", 1130, 120));
  fishparts.push(new FishPart("puzzle/14.png", 990, 170));
  fishparts.push(new FishPart("puzzle/15.png", 1130, 40));
  fishparts.push(new FishPart("puzzle/16.png", 1052, 420));
}

function draw() {
  background ('white');
  stroke(255, 204, 0);
  strokeWeight(4);
  rect(20,20,630, 1192)
  line(177, 20, 177, 1212);
  line(334, 20, 334, 1212);
  line(491, 20, 491, 1212);
  line(20, 318, 648, 318);
  line(20, 616, 648, 616);
  line(20, 914, 648, 914);

  var fishPartIsHovered = false;
  for (let index = fishparts.length - 1; index >= 0; index--) {
    if (fishparts[index].isHovered()) {
      fishPartIsHovered = true;
      break;
    }
  }
  if (heldFishPart === null ) {
    cursor(HAND);
  } else if ((fishPartIsHovered || heldFishPart !== null)) {
    cursor(MOVE);
  } else {
    cursor(ARROW);
  }

  if (heldFishPart !== null ) {
    heldFishPart.move(mouseX - pmouseX, mouseY - pmouseY);
  }


  for (let index = 0; index < fishparts.length; index++) {
    fishparts[index].draw();
  }
  
  if (heldFishPart !== null) {
    heldFishPart.draw();
  }


  if(isCompleted()){
    textSize(64);
    textAlign(CENTER, CENTER);
    fill(255);
    stroke(0);
    strokeWeight(4);
    text("Puzzle Completed!", width / 2, 200);
  }
  
}

function mousePressed() {
  if (mouseButton === LEFT) {
    for (let index = fishparts.length - 1; index >= 0; index--) {
      if (fishparts[index].isHovered()) {
        heldFishPart = fishparts[index];
        imagePos = index;
        // temporary remove image from array
        fishparts.splice(index, 1);
        break;
      }
    }
  }
}

function mouseReleased() {
  if (mouseButton === LEFT && heldFishPart !== null) {

    fishparts.splice(imagePos, 0, heldFishPart);
    heldFishPart = null;
  }
}

function isCompleted(){
  let sum = 0;
  for (let index = 0; index < fishparts.length; index++) {
    if(index == 0 && fishparts[index].x >= 15 && fishparts[index].x <= 25 && fishparts[index].y >= 15 && fishparts[index].y <= 25){
      sum += 1;
    }else if(index == 1 && fishparts[index].x >= 172 && fishparts[index].x <= 183 && fishparts[index].y >= 15 && fishparts[index].y <= 25){
      
      sum += 1;
    }else if(index == 2 && fishparts[index].x >= 329 && fishparts[index].x <= 340 && fishparts[index].y >= 15 && fishparts[index].y <= 25){
      
      sum += 1;
    }else if(index == 3 && fishparts[index].x >= 486 && fishparts[index].x <= 496 && fishparts[index].y >= 15 && fishparts[index].y <= 25){
      
      sum += 1;
    }else if(index == 4 && fishparts[index].x >= 15 && fishparts[index].x <= 25 && fishparts[index].y >= 313 && fishparts[index].y <= 323){
      
      sum += 1;
    }else if(index == 5 && fishparts[index].x >= 172 && fishparts[index].x <= 183 && fishparts[index].y >= 313 && fishparts[index].y <= 323){
      
      sum += 1;
    }else if(index == 6 && fishparts[index].x >= 329 && fishparts[index].x <= 340 && fishparts[index].y >= 313 && fishparts[index].y <= 323){
      
      sum += 1;
    }else if(index == 7 && fishparts[index].x >= 486 && fishparts[index].x <= 496 && fishparts[index].y >= 313 && fishparts[index].y <= 323){
      
      sum += 1;
    }else if(index == 8 && fishparts[index].x >= 15 && fishparts[index].x <= 25 && fishparts[index].y >= 611 && fishparts[index].y <= 621){
      
      sum += 1;
    }else if(index == 9 && fishparts[index].x >= 172 && fishparts[index].x <= 183 && fishparts[index].y >= 611 && fishparts[index].y <= 621){
      
      sum += 1;
    }else if(index == 10 && fishparts[index].x >= 329 && fishparts[index].x <= 340 && fishparts[index].y >= 611 && fishparts[index].y <= 621){
      
      sum += 1;
    }else if(index == 11 && fishparts[index].x >= 486 && fishparts[index].x <= 496 && fishparts[index].y >= 611 && fishparts[index].y <= 621){
      
      sum += 1;
    }else if(index == 12 && fishparts[index].x >= 15 && fishparts[index].x <= 25 && fishparts[index].y >= 909 && fishparts[index].y <= 919){
      
      sum += 1;
    }else if(index == 13 && fishparts[index].x >= 172 && fishparts[index].x <= 183 && fishparts[index].y >= 909 && fishparts[index].y <= 919){
      
      sum += 1;
    }else if(index == 14 && fishparts[index].x >= 329 && fishparts[index].x <= 340 && fishparts[index].y >= 909 && fishparts[index].y <= 919){
      
      sum += 1;
    }else if(index == 15 && fishparts[index].x >= 486 && fishparts[index].x <= 496 && fishparts[index].y >= 909 && fishparts[index].y <= 919){
      
      sum += 1;
    }

  }
  if(sum == 16){
    return true;
  }
}
