//Λογική αν γίνεται 
let song;
let song2;
let img;
let img2;

function preload() {
  song = loadSound('data/scrapey.wav');
  song2 = loadSound('data/tremolo_cut.wav');
  img = loadImage('data/Hell_D4.jpg');
  img2=loadImage('data/Hell_D4_gate.png');
}

function draw() {
  createCanvas(1000, 500);
  background(0);

  imageMode(CENTER);
  image(img, width/2, height/2);

  if (mouseX>620 && mouseX<670 && mouseY>270 && mouseY<400) {
    imageMode(CENTER);
    image(img2, width/2, height/2);
  }

  // Draw some lines to show what is going on
  stroke(200, 0, 0);
  line(0, mouseY, width, mouseY);
  stroke(200, 0, 0);
  line(mouseX, 0, mouseX, height);


  //fill(225);
  //text("(" + mouseX + ", " + mouseY + ")", mouseX, mouseY);

  if (mouseX>0 && mouseX<660) {
    song.play();
    let volume = map(mouseX, 660, 0, height, 0.1);
    song.volume = constrain(volume, 0, 0.1);
    song.amp(volume);
    song.pan(-1,0.0);
    song2.stop();
  } else if (mouseX>660 && mouseX<980) {
    song.stop();
    song2.play();
    let volume = map(mouseX, 660, 980, height, 0.1);
    song2.volume = constrain(volume, 0, 0.1);
    song2.amp(volume);
    song2.pan(0.0,1);
  } else {
    song.stop();
    song2.stop();
  }

    //αντίστοιχα οταν ειναι δεξια παιζει το 2ο κομμάτι--> πιο κοντα στο σημειο πιο δυνατα
  }
  function mousePressed() {
    let fs = fullscreen();
    fullscreen(!fs);
  }
