let capture;
let fire;
var Range;

function preload() {
  fire= createVideo (['data/f100.mp4']);
}

function setup() {
  capture = createCapture(VIDEO);
  capture.hide();
  fire.loop();
  fire.hide();
  while (fire.height == 0) delay(2);
}


function draw() {
  createCanvas(windowWidth, windowHeight);
  background(255);
  imageMode(CENTER);
  image(capture, width/2, height/4);
  filter(INVERT);
  tint(255, Range);
  Range= map(mouseX, 0, windowWidth, 0, 255);

  imageMode(CENTER);
  blendMode(LIGHTEST);
  image(fire, width/2, height/4);

  tint(255, 255);
}


function mousePressed() {
  let fs = fullscreen();
  fullscreen(!fs);
}
