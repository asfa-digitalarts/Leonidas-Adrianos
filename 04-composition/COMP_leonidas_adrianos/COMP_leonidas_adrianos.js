var HdPg1 = "Περπατάς στο δρόμο. Η ώρα είναι περασμένη. Δεν θα έπρεπε να είσαι έξω αλλά είχες ανάγκη λίγο καθαρό αέρα. Απέναντι σου διακρίνεις μια κομψή φιγούρα να έρχεται προς το μέρος σου.";
var GuidePg1 = "Press any key to continue";
var HdPg2 = "Κάτω απ’ τα φώτα του δρόμου ένα πανέμορφό πρόσωπο αναδύεται και σου χαμογελάει.";
var OptPg2 = "1.  Χαμογέλα πίσω\n 2.  Σκύψε το κεφάλι και φύγε, δεν έχεις όρεξη για τέτοια…";
var GuidePg2 = "Press 1 OR 2";
var HdPg3 = "«Καλησπέρα» λέει με μια μειλίχια φωνή. «Κανονικά δεν θα έπρεπε να είσαι έξω. Ούτε εγώ είναι αλήθεια. Αλλά μιας και συναντηθήκαμε τι θα έλεγες να περπατήσουμε μαζί;»";
var OptPg3 = "1.   Αρνήσου ευγενικά \n 2.  «Φυσικά. Θα μου άρεσε λίγη παρέα»";
var HdPg4 = "Συζητάτε ανέμελα για λίγη ώρα και το άτομο σου φαίνεται όλο και πιο ενδιαφέρον. «Θα σε ενδιέφερε να παίξουμε ένα παιχνίδι; Σου αρέσουν οι γρίφοι;» Κάπως διστακτικά, αλλά με την περιέργειά σου να κυριαρχεί δέχεσαι.";
var HdPg5 = "«Αν βρεις την σωστή απάντηση λοιπόν, θα σου εκπληρώσω μια ευχή. Αν όχι…» Στο πανέμορφο πρόσωπο ένα δαιμονικό χαμόγελο διαγράφεται και έχεις την αίσθηση πως τα δόντια είναι αφύσικα κοφτερά «θα πάρω την ψυχή σου».";
var OptPg5 = "1.  «Δεν υπάρχει περίπτωση να συμμετάσχω σε κάτι τέτοιο!» λες πισοπατώντας \n 2.  Με κρύο ιδρώτα να τρέχει στην πλάτη σου, γνέφεις ελαφρώς καταφατικά… ";
var HdPg6 = "«Είμαι ζωντανός όπως εσύ αλλά αναπνοή δεν παίρνω. Παγωμένος στην ζωή και στον θάνατο μένω. Ποτέ δεν διψώ μα συνέχεια πίνω. Ντυμένος με πανοπλία που ποτέ δεν ηχεί… Τι είμαι;»";
var GuidePg6 = "Μεταβείτε στον επόμενο κώδικα για την απάντηση";

var End1 = "Καθώς αποστρέφεις το βλέμμα σου για να φύγεις, βλέπεις μια ψυχρή έκφραση στο πρόσωπο του άλλου ατόμου.Γυρνάς σπίτι και ξαπλώνεις το κουρασμένο σου κορμί καθώς μια σπίθα περιέργειας τριβελίζει το μυαλό σου.";
var End2 = "Καθώς αποστρέφεις το βλέμμα σου για να φύγεις, βλέπεις μια ψυχρή έκφραση στο πρόσωπο του άλλου ατόμου. Πριν προλάβεις να  κάνεις δύο βήματα νιώθεις έναν οξύ πόνο στο στήθος σου. Κοιτάς κάτω και ένα χέρι εξέχει. Κρατάει την ακόμα παλλόμενη καρδία σου. «Αααχ… Πληγώνομαι πολύ όταν με απορρίπτουν» αναστέναξαν δυο χείλη στο αυτί σου. Το βλέμμα σου θολώνει και καθώς όλα σκοτεινιάζουν έχεις την αίσθηση πως η σκιά του ατόμου έχει αλλάξει παίρνοντας μια δαιμονική όψη…";

var pageNum = 1;
let r, g, b;
let song2btn=0;

let songevil=0;


function preload() {
  death = loadImage('data/Skull_smoke.jpg');
  bed = loadImage('data/bed.jpg');
  happy_demon = loadImage('data/end.jpg');
  song1 = loadSound('data/soft.mp3');
  song2 = loadSound('data/ambience.mp3');
  song2 = loadSound('data/ambience.mp3');
  bip = loadSound('data/bip.mp3');
  drama = loadSound('data/dramatic.mp3');
  evil = loadSound('data/evil_laugh.wav');
  //yawn= loadSound('data/yawn.mp3');
}

function setup() {
  song1.loop();
  song1.setVolume(0.8, 10);
  
  
}

function draw() {
  // Initialize variables for page title location
  HeadingX = windowWidth / 2;
  HeadingY = windowHeight / 4;
  //r=random(160,170);
  ////g=random(35);
  //b=random(62,50);


  // "Erase" screen after each frame
  createCanvas(windowWidth, windowHeight);

  background(160, 35, 42);

  // Draw page
  drawPage(pageNum);
}
//if key is pressed, got to the next assigned screen
function keyPressed() {
  if (key == 1 || key == 2) {
    bip.play();
  }
  if (pageNum == 1 || pageNum == 4) {
    pageNum = pageNum + 1;
    song2btn=1;
  } else if (pageNum == 2) {
    if (key == 1) {
      pageNum = pageNum + 1;
    } else if (key == 2) {
      pageNum = 7;
    }
  } else if (pageNum == 3) {
    if (key == 2) {
      pageNum = pageNum + 1;
    } else if (key == 1) {
      pageNum = 7;
    }
  } else if (pageNum == 5) {
    
    if (key == 2) {
      pageNum = pageNum + 1;
    } else if (key == 1) {
      pageNum = 8;
    }
  }

}


function drawPage(pageNum) {
  if (pageNum == 1) {
    textSize(22);
    textAlign(CENTER);
    fill(225);

    //"Content", Weidth, height
    text(HdPg1, width / 2 - 1000 / 2, HeadingY, 1000, 200);
    textSize(18);
    text(GuidePg1, HeadingX, HeadingY + 160);
  } else if (pageNum == 2) {
    textSize(22);
    text(HdPg2, width / 2 - 1000 / 2, HeadingY, 1000, 200);

    textSize(18);
    text(OptPg2, HeadingX, HeadingY + 90);
    // Guide text
    textSize(16);
    text(GuidePg2, HeadingX, HeadingY + 400);
  } else if (pageNum == 3) {
    textSize(22);
    text(HdPg3, width / 2 - 1000 / 2, HeadingY, 1000, 200);

    textSize(18);
    text(OptPg3, HeadingX, HeadingY + 120);
  } else if (pageNum == 4) {
    textSize(22);
    text(HdPg4, width / 2 - 1000 / 2, HeadingY, 1000, 200);

    // Guide text
    textSize(18);
    text(GuidePg1, HeadingX, HeadingY + 300);
  } else if (pageNum == 5) {
    textSize(22);
    text(HdPg5, width / 2 - 1000 / 2, HeadingY, 1000, 200);

    textSize(18);
    text(OptPg5, HeadingX, HeadingY + 180);

    if(song2btn ===1){
      
    song1.stop();
    song2.play();
    song2.amp(0.5);
    song2btn=0;
    }
    
    
  } else if (pageNum == 6) {
    imageMode(CENTER);
    image(happy_demon, width / 2, height - height / 2);

    textSize(22);
    text(HdPg6, width / 2 - 1000 / 2, HeadingY, 1000, 200);

    textSize(14);
    text(GuidePg6, HeadingX, HeadingY + 260);

    // song2.stop();
    evil.play();
    evil.volume(0.8);

  } else if (pageNum == 7) {
    background(104, 145, 151);
    imageMode(CENTER);
    image(bed, width / 2, height - height / 2);

    //yawn.play();
    textSize(18);
    text(End1, HeadingX, HeadingY);
  } else if (pageNum == 8) {
    createCanvas(windowWidth, windowHeight);
    background(0);
    imageMode(CENTER);
    image(death, width / 2, height - height / 3);
    textSize(18);
    fill(255, 0, 0);
    text(End2, width / 2 - 1000 / 2, HeadingY, 1000, 200);

    song2.stop();
    drama.play();
    drama.volume(0.5);
  }
}


function mousePressed() {
  let fs = fullscreen();
  fullscreen(!fs);
}
